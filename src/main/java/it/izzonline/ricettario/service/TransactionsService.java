package it.izzonline.ricettario.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.micrometer.core.instrument.util.StringUtils;
import it.izzonline.ricettario.objects.Ingrediente;
import it.izzonline.ricettario.objects.Meta;
import it.izzonline.ricettario.objects.Post;
import it.izzonline.ricettario.objects.Ricetta;
import it.izzonline.ricettario.objects.RicettaWordPress;
import it.izzonline.ricettario.objects.Step;
import it.izzonline.ricettario.objects.User;
import it.izzonline.ricettario.objects.WpId;
import it.izzonline.ricettario.repository.RicettaRepository;

@Service
public class TransactionsService {

	private static final String DRAFT = "draft";
	private static final String TOKEN_URL = "http://knifeandforkrecipes.izzonline.it/wp-json/jwt-auth/v1/token";
	private static final String POST_TO_WP_URL = "http://knifeandforkrecipes.izzonline.it/wp-json/wp/v2/posts";

	// CATEGORIE DI RICETTARIO-BIMBY e TAG in K&F RECIPES
	private static final String CATEGORIA_DESSERT = "Dessert e pralineria";
	private static final String CATEGORIA_BASE = "Ricette base";
	private static final String CATEGORIA_DOLCI = "Prodotti da forno dolci";
	private static final String CATEGORIA_SALATI = "Prodotti da forno salati";
	private static final String CATEGORIA_PRIMI = "Primi piatti";
	private static final String CATEGORIA_DRINKS = "Bibite liquori e bevande";
	private static final String CATEGORIA_SOUP = "Zuppe passati e minestre";
	private static final String CATEGORIA_SECONDI = "Secondi piatti a base di carne e salumi";
	private static final String CATEGORIA_UNICI = "Piatti unici";
	private static final String CATEGORIA_SALSE = "Salse sughi condimenti creme spalmabili e confetture";
	private static final String CATEGORIA_ANTIPASTI = "Antipasti";
	private static final String CATEGORIA_CONTORNI = "Contorni";
	private static final String CATEGORIA_VEGETARIANI = "Secondi piatti vegetariani";
	private static final String CATEGORIA_PANE = "Pane";
	private static final String CATEGORIA_SEAFOOD = "Secondi piatti a base di pesce";
	private static final String CATEGORIA_BABY = "Alimentazione infantile";

	private static final Integer TAG_DESSERT = 100;
	private static final Integer TAG_BASE = 101;
	private static final Integer TAG_DOLCI = 102;
	private static final Integer TAG_SALATI = 103;
	private static final Integer TAG_PRIMI = 104;
	private static final Integer TAG_DRINKS = 105;
	private static final Integer TAG_SOUP = 106;
	private static final Integer TAG_SECONDI = 107;
	private static final Integer TAG_UNICI = 108;
	private static final Integer TAG_SALSE = 109;
	private static final Integer TAG_ANTIPASTI = 110;
	private static final Integer TAG_CONTORNI = 111;
	private static final Integer TAG_VEGETARIANI = 112;
	private static final Integer TAG_PANE = 113;
	private static final Integer TAG_SEAFOOD = 114;
	private static final Integer TAG_BABY = 115;

	// CATEGORIE DI KNIFE AND FORK RECIPES
	private static final Integer CATEGORY_GENERAL = 96;
	private static final Integer CATEGORY_MAIN = 3;
	private static final Integer CATEGORY_SALADS = 6;
	private static final Integer CATEGORY_DESSERT = 2;
	private static final Integer CATEGORY_PIZZA = 5;
	private static final Integer CATEGORY_SOUP = 97;
	private static final Integer CATEGORY_SEAFOOD = 98;
	private static final Integer CATEGORY_DRINKS = 99;
	private static final Integer CATEGORY_NONE = 1; // senza categoria

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RicettaRepository ricettaRepository;

	public String updateRecipe(RicettaWordPress ricettaWP) {
		Ricetta ricetta = ricettaRepository.findByWpId(ricettaWP.getId());

		if (ricetta != null) {

			ricetta.setIngredienti(null);
			ricetta.setStepList(null);

			Ricetta newRecipe = ricetta;

			List<Ingrediente> ingredienti = new ArrayList<Ingrediente>();
			Ingrediente i;

			if (!StringUtils.isBlank(ricettaWP.getIngredient1())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient1());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient2())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient2());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient3())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient3());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient4())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient4());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient5())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient5());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient6())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient6());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient7())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient7());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient8())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient8());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient9())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient9());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient10())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient10());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient11())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient11());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient12())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient12());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient13())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient13());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient14())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient14());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient15())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient15());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient16())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient16());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient17())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient17());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient18())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient18());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient19())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient19());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient20())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient20());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient21())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient21());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient22())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient22());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient23())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient23());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient24())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient24());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient25())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient25());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient26())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient26());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient27())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient27());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient28())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient28());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient29())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient29());
				ingredienti.add(i);
			}

			if (!StringUtils.isBlank(ricettaWP.getIngredient30())) {
				i = new Ingrediente();
				i.setRicetta(newRecipe);
				i.setText(ricettaWP.getIngredient30());
				ingredienti.add(i);
			}

			List<Step> stepList = new ArrayList<Step>();
			Step s;

			if (!StringUtils.isBlank(ricettaWP.getStep1())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep1());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep2())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep2());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep3())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep3());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep4())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep4());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep5())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep5());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep6())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep6());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep7())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep7());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep8())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep8());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep9())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep9());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep10())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep10());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep11())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep11());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep12())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep12());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep13())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep13());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep14())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep14());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep15())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep15());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep16())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep16());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep17())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep17());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep18())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep18());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep19())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep19());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep20())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep20());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep21())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep21());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep22())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep22());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep23())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep23());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep24())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep24());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep25())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep25());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep26())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep26());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep27())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep27());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep28())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep28());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep29())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep29());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getStep30())) {
				s = new Step();
				s.setRicetta(newRecipe);
				s.setStepText(ricettaWP.getStep30());
				stepList.add(s);
			}

			if (!StringUtils.isBlank(ricettaWP.getCategory())) {
				newRecipe.setCategoria(ricettaWP.getCategory());
			}

			if (!StringUtils.isBlank(ricettaWP.getCooking_time())) {
				newRecipe.setCookingTime(ricettaWP.getCooking_time());
			}

			if (!StringUtils.isBlank(ricettaWP.getImage())) {
				newRecipe.setImage(ricettaWP.getImage());
			}

			newRecipe.setTitle(ricettaWP.getTitle());

			newRecipe.setIngredienti(ingredienti);
			newRecipe.setStepList(stepList);

			ricettaRepository.delete(ricetta);
			ricettaRepository.save(newRecipe);

		}
		return "Ricetta aggiornata correttamente";
	}

//	@Transactional
//	public String deleteOrphans() {
//
//		stepRepository.deleteOrphans();
//		ingredienteRepository.deleteOrphans();
//
//		return "Elementi orfani cancellati correttamente";
//	}

	public String exportRecipes(User user) {

		String accessToken = getAccessToken(user);
		List<Ricetta> ricette = ricettaRepository.findByWpIdIsNull();
		for (Ricetta ricetta : ricette) {
			Post post = new Post();
			post.setTitle(ricetta.getTitle());
			post.setStatus(DRAFT);
			String categoria = ricetta.getCategoria();

			List<Integer> categories = new ArrayList<Integer>();
			List<Integer> tags = new ArrayList<>();

			if (categoria.equals(CATEGORIA_ANTIPASTI) || categoria.equals(CATEGORIA_CONTORNI)
					|| categoria.equals(CATEGORIA_VEGETARIANI)) {
				categories.add(CATEGORY_SALADS);
				if (categoria.equals(CATEGORIA_ANTIPASTI)) {
					tags.add(TAG_ANTIPASTI);
				}
				if (categoria.equals(CATEGORIA_CONTORNI)) {
					tags.add(TAG_CONTORNI);
				}
				if (categoria.equals(CATEGORIA_VEGETARIANI)) {
					tags.add(TAG_VEGETARIANI);
				}
			} else if (categoria.equals(CATEGORIA_BABY) || categoria.equals(CATEGORIA_BASE)
					|| categoria.equals(CATEGORIA_PANE)) {
				categories.add(CATEGORY_GENERAL);
				if (categoria.equals(CATEGORIA_BABY)) {
					tags.add(TAG_BABY);
				}
				if (categoria.equals(CATEGORIA_BASE)) {
					tags.add(TAG_BASE);
				}
				if (categoria.equals(CATEGORIA_PANE)) {
					tags.add(TAG_PANE);
				}
			} else if (categoria.equals(CATEGORIA_UNICI) || categoria.equals(CATEGORIA_PRIMI)
					|| categoria.equals(CATEGORIA_SECONDI)) {
				categories.add(CATEGORY_MAIN);
				if (categoria.equals(CATEGORIA_UNICI)) {
					tags.add(TAG_UNICI);
				}
				if (categoria.equals(CATEGORIA_PRIMI)) {
					tags.add(TAG_PRIMI);
				}
				if (categoria.equals(CATEGORIA_SECONDI)) {
					tags.add(TAG_SECONDI);
				}
			} else if (categoria.equals(CATEGORIA_DESSERT) || categoria.equals(CATEGORIA_DOLCI)) {
				categories.add(CATEGORY_DESSERT);
				if (categoria.equals(CATEGORIA_DESSERT)) {
					tags.add(TAG_DESSERT);
				}
				if (categoria.equals(CATEGORIA_DOLCI)) {
					tags.add(TAG_DOLCI);
				}
			} else if (categoria.equals(CATEGORIA_SALSE) || categoria.equals(CATEGORIA_SOUP)) {
				categories.add(CATEGORY_SOUP);
				if (categoria.equals(CATEGORIA_SALSE)) {
					tags.add(TAG_SALSE);
				}
				if (categoria.equals(CATEGORIA_SOUP)) {
					tags.add(TAG_SOUP);
				}
			} else if (categoria.equals(CATEGORIA_SALATI)) {
				categories.add(CATEGORY_PIZZA);
				tags.add(TAG_SALATI);
			} else if (categoria.equals(CATEGORIA_SEAFOOD)) {
				categories.add(CATEGORY_SEAFOOD);
				tags.add(TAG_SEAFOOD);
			} else if (categoria.equals(CATEGORIA_DRINKS)) {
				categories.add(CATEGORY_DRINKS);
				tags.add(TAG_DRINKS);
			} else {
				categories.add(CATEGORY_NONE);

			}
			post.setCategories(categories);
			post.setTags(tags);

			// REMEMBER l'immagine non viene esportata in WP.
			// Quando si deve aggiornare il DB rest se in WP ne è stata impostata una nuova
			// quella vecchia viene sovrascritta altrimenti resta quella vecchia.
			// IDEM per il rating!

			Meta meta = new Meta();
			meta.setCategory(ricetta.getCategoria());
			meta.setCooking_time(ricetta.getCookingTime());

			List<Ingrediente> ingredienti = ricetta.getIngredienti();
			int count = 1;
			for (Ingrediente i : ingredienti) {
				if (count == 1)
					meta.setIngredient1(i.getText());
				if (count == 2)
					meta.setIngredient2(i.getText());
				if (count == 3)
					meta.setIngredient3(i.getText());
				if (count == 4)
					meta.setIngredient4(i.getText());
				if (count == 5)
					meta.setIngredient5(i.getText());
				if (count == 6)
					meta.setIngredient6(i.getText());
				if (count == 7)
					meta.setIngredient7(i.getText());
				if (count == 8)
					meta.setIngredient8(i.getText());
				if (count == 9)
					meta.setIngredient9(i.getText());
				if (count == 10)
					meta.setIngredient10(i.getText());
				if (count == 11)
					meta.setIngredient11(i.getText());
				if (count == 12)
					meta.setIngredient12(i.getText());
				if (count == 13)
					meta.setIngredient13(i.getText());
				if (count == 14)
					meta.setIngredient14(i.getText());
				if (count == 15)
					meta.setIngredient15(i.getText());
				if (count == 16)
					meta.setIngredient16(i.getText());
				if (count == 17)
					meta.setIngredient17(i.getText());
				if (count == 18)
					meta.setIngredient18(i.getText());
				if (count == 19)
					meta.setIngredient19(i.getText());
				if (count == 20)
					meta.setIngredient20(i.getText());
				if (count == 21)
					meta.setIngredient21(i.getText());
				if (count == 22)
					meta.setIngredient22(i.getText());
				if (count == 23)
					meta.setIngredient23(i.getText());
				if (count == 24)
					meta.setIngredient24(i.getText());
				if (count == 25)
					meta.setIngredient25(i.getText());
				if (count == 26)
					meta.setIngredient26(i.getText());
				if (count == 27)
					meta.setIngredient27(i.getText());
				if (count == 28)
					meta.setIngredient28(i.getText());
				if (count == 29)
					meta.setIngredient29(i.getText());
				if (count == 30)
					meta.setIngredient30(i.getText());
				count++;
			}

			count = 1;
			List<Step> steps = ricetta.getStepList();
			for (Step s : steps) {
				if (count == 1)
					meta.setStep1(s.getStepText());
				if (count == 2)
					meta.setStep2(s.getStepText());
				if (count == 3)
					meta.setStep3(s.getStepText());
				if (count == 4)
					meta.setStep4(s.getStepText());
				if (count == 5)
					meta.setStep5(s.getStepText());
				if (count == 6)
					meta.setStep6(s.getStepText());
				if (count == 7)
					meta.setStep7(s.getStepText());
				if (count == 8)
					meta.setStep8(s.getStepText());
				if (count == 9)
					meta.setStep9(s.getStepText());
				if (count == 10)
					meta.setStep10(s.getStepText());
				if (count == 11)
					meta.setStep11(s.getStepText());
				if (count == 12)
					meta.setStep12(s.getStepText());
				if (count == 13)
					meta.setStep13(s.getStepText());
				if (count == 14)
					meta.setStep14(s.getStepText());
				if (count == 15)
					meta.setStep15(s.getStepText());
				if (count == 16)
					meta.setStep16(s.getStepText());
				if (count == 17)
					meta.setStep17(s.getStepText());
				if (count == 18)
					meta.setStep18(s.getStepText());
				if (count == 19)
					meta.setStep19(s.getStepText());
				if (count == 20)
					meta.setStep20(s.getStepText());
				if (count == 21)
					meta.setStep21(s.getStepText());
				if (count == 22)
					meta.setStep22(s.getStepText());
				if (count == 23)
					meta.setStep23(s.getStepText());
				if (count == 24)
					meta.setStep24(s.getStepText());
				if (count == 25)
					meta.setStep25(s.getStepText());
				if (count == 26)
					meta.setStep26(s.getStepText());
				if (count == 27)
					meta.setStep27(s.getStepText());
				if (count == 28)
					meta.setStep28(s.getStepText());
				if (count == 29)
					meta.setStep29(s.getStepText());
				if (count == 30)
					meta.setStep30(s.getStepText());
				count++;
			}
			post.setMeta(meta);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("accept", MediaType.APPLICATION_JSON.toString());
			headers.set("Authorization", "Bearer " + accessToken);
			HttpEntity<Post> request = new HttpEntity<>(post, headers);
			WpId createdPost = restTemplate.postForObject(POST_TO_WP_URL, request, WpId.class);

			// UPDATE RICETTA DATABASE SET wpId
			ricetta.setWpId(createdPost.getId());
			ricettaRepository.save(ricetta);
		}

		return "Export completato correttamente";
	}

	private String getAccessToken(User user) {
		HttpEntity<User> request = new HttpEntity<>(user);
		User retVal = restTemplate.postForObject(TOKEN_URL, request, User.class);
		return retVal.getToken();
	}

}
