package it.izzonline.ricettario.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Meta {

	private String category;
	private String cooking_time;
	private String step1;
	private String step2;
	private String step3;
	private String step4;
	private String step5;
	private String step6;
	private String step7;
	private String step8;
	private String step9;
	private String step10;
	private String step11;
	private String step12;
	private String step13;
	private String step14;
	private String step15;
	private String step16;
	private String step17;
	private String step18;
	private String step19;
	private String step20;
	private String step21;
	private String step22;
	private String step23;
	private String step24;
	private String step25;
	private String step26;
	private String step27;
	private String step28;
	private String step29;
	private String step30;
	private String ingredient1;
	private String ingredient2;
	private String ingredient3;
	private String ingredient4;
	private String ingredient5;
	private String ingredient6;
	private String ingredient7;
	private String ingredient8;
	private String ingredient9;
	private String ingredient10;
	private String ingredient11;
	private String ingredient12;
	private String ingredient13;
	private String ingredient14;
	private String ingredient15;
	private String ingredient16;
	private String ingredient17;
	private String ingredient18;
	private String ingredient19;
	private String ingredient20;
	private String ingredient21;
	private String ingredient22;
	private String ingredient23;
	private String ingredient24;
	private String ingredient25;
	private String ingredient26;
	private String ingredient27;
	private String ingredient28;
	private String ingredient29;
	private String ingredient30;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStep1() {
		return step1;
	}

	public void setStep1(String step1) {
		this.step1 = step1;
	}

	public String getStep2() {
		return step2;
	}

	public void setStep2(String step2) {
		this.step2 = step2;
	}

	public String getStep3() {
		return step3;
	}

	public void setStep3(String step3) {
		this.step3 = step3;
	}

	public String getStep4() {
		return step4;
	}

	public void setStep4(String step4) {
		this.step4 = step4;
	}

	public String getStep5() {
		return step5;
	}

	public void setStep5(String step5) {
		this.step5 = step5;
	}

	public String getStep6() {
		return step6;
	}

	public void setStep6(String step6) {
		this.step6 = step6;
	}

	public String getStep7() {
		return step7;
	}

	public void setStep7(String step7) {
		this.step7 = step7;
	}

	public String getStep8() {
		return step8;
	}

	public void setStep8(String step8) {
		this.step8 = step8;
	}

	public String getStep9() {
		return step9;
	}

	public void setStep9(String step9) {
		this.step9 = step9;
	}

	public String getStep10() {
		return step10;
	}

	public void setStep10(String step10) {
		this.step10 = step10;
	}

	public String getStep11() {
		return step11;
	}

	public void setStep11(String step11) {
		this.step11 = step11;
	}

	public String getStep12() {
		return step12;
	}

	public void setStep12(String step12) {
		this.step12 = step12;
	}

	public String getStep13() {
		return step13;
	}

	public void setStep13(String step13) {
		this.step13 = step13;
	}

	public String getStep14() {
		return step14;
	}

	public void setStep14(String step14) {
		this.step14 = step14;
	}

	public String getStep15() {
		return step15;
	}

	public void setStep15(String step15) {
		this.step15 = step15;
	}

	public String getIngredient1() {
		return ingredient1;
	}

	public void setIngredient1(String ingredient1) {
		this.ingredient1 = ingredient1;
	}

	public String getIngredient2() {
		return ingredient2;
	}

	public void setIngredient2(String ingredient2) {
		this.ingredient2 = ingredient2;
	}

	public String getIngredient3() {
		return ingredient3;
	}

	public void setIngredient3(String ingredient3) {
		this.ingredient3 = ingredient3;
	}

	public String getIngredient4() {
		return ingredient4;
	}

	public void setIngredient4(String ingredient4) {
		this.ingredient4 = ingredient4;
	}

	public String getIngredient5() {
		return ingredient5;
	}

	public void setIngredient5(String ingredient5) {
		this.ingredient5 = ingredient5;
	}

	public String getIngredient6() {
		return ingredient6;
	}

	public void setIngredient6(String ingredient6) {
		this.ingredient6 = ingredient6;
	}

	public String getIngredient7() {
		return ingredient7;
	}

	public void setIngredient7(String ingredient7) {
		this.ingredient7 = ingredient7;
	}

	public String getIngredient8() {
		return ingredient8;
	}

	public void setIngredient8(String ingredient8) {
		this.ingredient8 = ingredient8;
	}

	public String getIngredient9() {
		return ingredient9;
	}

	public void setIngredient9(String ingredient9) {
		this.ingredient9 = ingredient9;
	}

	public String getIngredient10() {
		return ingredient10;
	}

	public void setIngredient10(String ingredient10) {
		this.ingredient10 = ingredient10;
	}

	public String getIngredient11() {
		return ingredient11;
	}

	public void setIngredient11(String ingredient11) {
		this.ingredient11 = ingredient11;
	}

	public String getIngredient12() {
		return ingredient12;
	}

	public void setIngredient12(String ingredient12) {
		this.ingredient12 = ingredient12;
	}

	public String getIngredient13() {
		return ingredient13;
	}

	public void setIngredient13(String ingredient13) {
		this.ingredient13 = ingredient13;
	}

	public String getIngredient14() {
		return ingredient14;
	}

	public void setIngredient14(String ingredient14) {
		this.ingredient14 = ingredient14;
	}

	public String getIngredient15() {
		return ingredient15;
	}

	public void setIngredient15(String ingredient15) {
		this.ingredient15 = ingredient15;
	}

	public String getStep16() {
		return step16;
	}

	public void setStep16(String step16) {
		this.step16 = step16;
	}

	public String getStep17() {
		return step17;
	}

	public void setStep17(String step17) {
		this.step17 = step17;
	}

	public String getStep18() {
		return step18;
	}

	public void setStep18(String step18) {
		this.step18 = step18;
	}

	public String getStep19() {
		return step19;
	}

	public void setStep19(String step19) {
		this.step19 = step19;
	}

	public String getStep20() {
		return step20;
	}

	public void setStep20(String step20) {
		this.step20 = step20;
	}

	public String getStep21() {
		return step21;
	}

	public void setStep21(String step21) {
		this.step21 = step21;
	}

	public String getStep22() {
		return step22;
	}

	public void setStep22(String step22) {
		this.step22 = step22;
	}

	public String getStep23() {
		return step23;
	}

	public void setStep23(String step23) {
		this.step23 = step23;
	}

	public String getStep24() {
		return step24;
	}

	public void setStep24(String step24) {
		this.step24 = step24;
	}

	public String getStep25() {
		return step25;
	}

	public void setStep25(String step25) {
		this.step25 = step25;
	}

	public String getStep26() {
		return step26;
	}

	public void setStep26(String step26) {
		this.step26 = step26;
	}

	public String getStep27() {
		return step27;
	}

	public void setStep27(String step27) {
		this.step27 = step27;
	}

	public String getStep28() {
		return step28;
	}

	public void setStep28(String step28) {
		this.step28 = step28;
	}

	public String getStep29() {
		return step29;
	}

	public void setStep29(String step29) {
		this.step29 = step29;
	}

	public String getStep30() {
		return step30;
	}

	public void setStep30(String step30) {
		this.step30 = step30;
	}

	public String getIngredient16() {
		return ingredient16;
	}

	public void setIngredient16(String ingredient16) {
		this.ingredient16 = ingredient16;
	}

	public String getIngredient17() {
		return ingredient17;
	}

	public void setIngredient17(String ingredient17) {
		this.ingredient17 = ingredient17;
	}

	public String getIngredient18() {
		return ingredient18;
	}

	public void setIngredient18(String ingredient18) {
		this.ingredient18 = ingredient18;
	}

	public String getIngredient19() {
		return ingredient19;
	}

	public void setIngredient19(String ingredient19) {
		this.ingredient19 = ingredient19;
	}

	public String getIngredient20() {
		return ingredient20;
	}

	public void setIngredient20(String ingredient20) {
		this.ingredient20 = ingredient20;
	}

	public String getIngredient21() {
		return ingredient21;
	}

	public void setIngredient21(String ingredient21) {
		this.ingredient21 = ingredient21;
	}

	public String getIngredient22() {
		return ingredient22;
	}

	public void setIngredient22(String ingredient22) {
		this.ingredient22 = ingredient22;
	}

	public String getIngredient23() {
		return ingredient23;
	}

	public void setIngredient23(String ingredient23) {
		this.ingredient23 = ingredient23;
	}

	public String getIngredient24() {
		return ingredient24;
	}

	public void setIngredient24(String ingredient24) {
		this.ingredient24 = ingredient24;
	}

	public String getIngredient25() {
		return ingredient25;
	}

	public void setIngredient25(String ingredient25) {
		this.ingredient25 = ingredient25;
	}

	public String getIngredient26() {
		return ingredient26;
	}

	public void setIngredient26(String ingredient26) {
		this.ingredient26 = ingredient26;
	}

	public String getIngredient27() {
		return ingredient27;
	}

	public void setIngredient27(String ingredient27) {
		this.ingredient27 = ingredient27;
	}

	public String getIngredient28() {
		return ingredient28;
	}

	public void setIngredient28(String ingredient28) {
		this.ingredient28 = ingredient28;
	}

	public String getIngredient29() {
		return ingredient29;
	}

	public void setIngredient29(String ingredient29) {
		this.ingredient29 = ingredient29;
	}

	public String getIngredient30() {
		return ingredient30;
	}

	public void setIngredient30(String ingredient30) {
		this.ingredient30 = ingredient30;
	}

	public String getCooking_time() {
		return cooking_time;
	}

	public void setCooking_time(String cooking_time) {
		this.cooking_time = cooking_time;
	}

}
