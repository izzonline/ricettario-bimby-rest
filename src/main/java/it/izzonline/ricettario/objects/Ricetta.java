package it.izzonline.ricettario.objects;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ricetta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6021253812209727404L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty
	private String uuid;

	@JsonProperty
	private Long wpId;

	@JsonProperty
	@Column(unique = true, nullable = false)
	private String title;

	@JsonProperty
	private String image;

	@JsonProperty
	private Integer rating;

	@JsonProperty
	private String cookingTime;

	@JsonProperty
	@OneToMany(mappedBy = "ricetta", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Step> stepList;

	@JsonProperty
	@OneToMany(mappedBy = "ricetta", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Ingrediente> ingredienti;

	@JsonProperty
	private String url;

	@JsonProperty
	@NotNull
	private String categoria;

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getCookingTime() {
		return cookingTime;
	}

	public void setCookingTime(String cookingTime) {
		this.cookingTime = cookingTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public List<Step> getStepList() {
		return stepList;
	}

	public void setStepList(List<Step> stepList) {
		this.stepList = stepList;
	}

	public List<Ingrediente> getIngredienti() {
		return ingredienti;
	}

	public void setIngredienti(List<Ingrediente> ingredienti) {
		this.ingredienti = ingredienti;
	}

	public Long getWpId() {
		return wpId;
	}

	public void setWpId(Long wpId) {
		this.wpId = wpId;
	}



	public Ricetta(Long id, String uuid, Long wpId, String title, String image, Integer rating, String cookingTime,
			List<Step> stepList, List<Ingrediente> ingredienti, String url, String categoria) {
		super();
		this.id = id;
		this.uuid = uuid;
		this.wpId = wpId;
		this.title = title;
		this.image = image;
		this.rating = rating;
		this.cookingTime = cookingTime;
		this.stepList = stepList;
		this.ingredienti = ingredienti;
		this.url = url;
		this.categoria = categoria;
	}

	public Ricetta() {
		super();
	}

}
