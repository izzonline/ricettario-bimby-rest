package it.izzonline.ricettario;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.izzonline.ricettario.objects.User;
import it.izzonline.ricettario.service.TransactionsService;

@Component
public class ScheduledTasks {

	@Autowired
	TransactionsService txService;

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	// scheduled at 10:15 AM on the 15th day of every month
	@Scheduled(cron = "0 15 10 15 * ?")
	public void scheduleTaskWithFixedDelay() {
		logger.info("BATCH IMPORT INIZIATO :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
		try {
			User user = new User();
			user.setUsername("stefania.izzo@me.com");
			user.setPassword("Ndrgrnd!680");
			logger.info(txService.exportRecipes(user));
			//logger.info(txService.deleteOrphans());
		} catch (Exception ex) {
			logger.error("Ran into an error {}", ex);
			throw new IllegalStateException(ex);
		}
	}

}
