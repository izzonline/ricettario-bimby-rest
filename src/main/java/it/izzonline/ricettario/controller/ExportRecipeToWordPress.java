package it.izzonline.ricettario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.izzonline.ricettario.objects.User;
import it.izzonline.ricettario.service.TransactionsService;

@RestController
public class ExportRecipeToWordPress {

	@Autowired
	private TransactionsService txService;

	@PostMapping("/export-to-wordpress")
	public String exportRecipes(@RequestBody(required = true) User user) {
		return txService.exportRecipes(user);
	}

}
