package it.izzonline.ricettario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.izzonline.ricettario.objects.RicettaWordPress;
import it.izzonline.ricettario.service.TransactionsService;

@RestController
public class UpdateRecipeFromWordPress {

	@Autowired
	TransactionsService txService;

	@CrossOrigin
	@PostMapping("/update")
	public void updateRecipe(@RequestBody RicettaWordPress ricettaWP) {
		txService.updateRecipe(ricettaWP);
	}

}
