package it.izzonline.ricettario.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.izzonline.ricettario.RicettarioBimbyRestApplication;
import it.izzonline.ricettario.objects.Ingrediente;
import it.izzonline.ricettario.objects.Ricetta;
import it.izzonline.ricettario.objects.Step;
import it.izzonline.ricettario.repository.RicettaRepository;

@RestController
public class HomeController {

	private static final String url = "https://www.ricettario-bimby.it/recipes/ajax/suggest-recipes";

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	RicettaRepository ricettaRepository;

	/**
	 * Metodo chiamato da Alexa per cercare le ricette
	 * 
	 * @param term
	 * @param pageRequest
	 * @return
	 */
	@GetMapping(value = { "/" }, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getRicette(@RequestParam(name = "term", required = true) String term,
			@RequestParam(name = "categoria", required = false) String categoria, Pageable pageRequest) {
		System.out.println("getRicette(" + term + ", " + categoria + ")");
		List<Ricetta> retVal = new ArrayList<>();
		try {
			retVal = cercaSuDatabase(term, categoria, pageRequest);
			if (retVal == null || retVal.size() == 0) {
				if (ottieniNuoveRicetteDaRicettarioBimby(term))
					retVal = cercaSuDatabase(term, categoria, pageRequest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	/**
	 * Alexa, quali sono le categorie?
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = { "/categorie" }, produces = "application/json")
	public @ResponseBody List<String> getCategorie() throws Exception {
		System.out.println("getCategorie()");
		return ricettaRepository.findCategorie();
	}

	/**
	 * QUI VA NEL CASO DI ELEMENT SELECTED. La ricetta esiste già nel db e la si
	 * cerca direttamente tramite ID
	 * 
	 * @param idRicetta
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = { "/{idRicetta}" }, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getRicetta(@PathVariable(name = "idRicetta", required = true) Long idRicetta)
			throws Exception {
		System.out.println("getRicetta(" + idRicetta + ")");
		Optional<Ricetta> ricetta = ricettaRepository.findById(idRicetta);
		return new ResponseEntity<>(ricetta.get(), HttpStatus.OK);
	}

	/**
	 * Riavvio remoto
	 */
	@PostMapping("/restart")
	public void restart() {
		System.out.println("restart()");
		RicettarioBimbyRestApplication.restart();
	}

	// PRIVATE METHODS

	/**
	 * Cerca su database
	 * 
	 * @param term
	 * @return
	 */
	private List<Ricetta> cercaSuDatabase(String term, String categoria, Pageable pageRequest) {
		System.out.println("cercaSuDatabase(" + term + ")");
		List<Ricetta> retVal = new ArrayList<>();
		Ricetta ricetta = null;
		if (StringUtils.isBlank(categoria)) {
			ricetta = ricettaRepository.findByTitle(term);
		} else {
			ricetta = ricettaRepository.findByTitleAndCategoria(term, categoria);
		}
		if (ricetta == null) {
			if (StringUtils.isBlank(categoria)) {
				retVal = ricettaRepository.findByTitleStartingWith(term, pageRequest);
			} else {
				retVal = ricettaRepository.findByTitleStartingWithAndCategoria(term, categoria, pageRequest);
			}
			if (retVal.size() == 0) {
				if (StringUtils.isBlank(categoria)) {
					retVal = ricettaRepository.findByTitleContaining(term, pageRequest);
				} else {
					retVal = ricettaRepository.findByTitleContainingAndCategoria(term, categoria, pageRequest);
				}
			}
		} else {
			retVal.add(ricetta);
		}
		return retVal;
	}

	/**
	 * Cerca su ricettario-bimby.it
	 * 
	 * @param term
	 * @return
	 * @throws IOException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 */
	private boolean ottieniNuoveRicetteDaRicettarioBimby(String term)
			throws IOException, JsonParseException, JsonMappingException {
		System.out.println("ottieniNuoveRicetteDaRicettarioBimby(" + term + ")");

		ResponseEntity<String> response = restTemplate.getForEntity(url + "?term=" + term, String.class);

		ObjectMapper mapper = new ObjectMapper();

		List<Ricetta> ricette = mapper.readValue(response.getBody(), new TypeReference<List<Ricetta>>() {
		});

		for (Ricetta ricetta : ricette) {

			String uuid = StringUtils.EMPTY;
			String categoria = StringUtils.EMPTY;
			String title = StringUtils.EMPTY;

			if (!StringUtil.isBlank(ricetta.getUrl()) && !StringUtil.isBlank(ricetta.getImage())
					&& ricetta.getRating() > 3) {

				uuid = StringUtils.substringAfterLast(ricetta.getUrl(), "/");

				// String regex = "^[a-zA-Z0-9 ùèòàì]+$";
				// Pattern pattern = Pattern.compile(regex);
				title = StringUtils.capitalize(ricetta.getTitle().toLowerCase());
				// Matcher matcher = pattern.matcher(title);

				Ricetta ricettaDatabase = ricettaRepository.findByUuid(uuid);
				Ricetta ricettaSameTitle = ricettaRepository.findByTitle(title);

				if (ricettaDatabase == null && ricettaSameTitle == null
				// && matcher.matches()
				) {

					ricetta.setTitle(title);
					ricetta.setUuid(uuid);

					categoria = StringUtils.substringAfter(ricetta.getUrl(), "https://www.ricettario-bimby.it/");
					categoria = StringUtils.substringBefore(categoria, "-ricette/");
					categoria = StringUtils.replace(categoria, "-", " ");
					categoria = StringUtils.capitalize(categoria);

					ricetta.setCategoria(categoria);

					Document doc = Jsoup.connect(ricetta.getUrl()).get();

					Elements images = doc.select("img");
					for (Element element : images) {
						String src = element.attr("src");

						if (src.contains("/bundles/tmrcfront/images/tinymce_icons/counterclock.png")) {
							element.html("<span>senso antiorario</span>");
						}
						if (src.contains("/bundles/tmrcfront/images/tinymce_icons/soft.png")) {
							element.html("<span>velocità soft</span>");
						}
						if (src.contains("/bundles/tmrcfront/images/tinymce_icons/nocounterclock.png")) {
							element.html("<span>senso orario</span>");
						}
						if (src.contains("/bundles/tmrcfront/images/tinymce_icons/corn.png")) {
							element.html("<span>velocità spiga</span>");
						}
						if (src.contains("/bundles/tmrcfront/images/tinymce_icons/locked.png")) {
							element.html("<span>boccale</span>");
						}

					}

					Elements elementsByClass = doc.body().getElementsByClass("steps-list");
					Elements liStepList = elementsByClass.select("li");

					List<Step> stepList = new ArrayList<Step>();

					if (liStepList.hasAttr("style")) {
						// list-style-type:none
						Elements p = elementsByClass.select("p");

						for (Element element : p) {

							String text = element.text();
							if (text != null && !text.trim().equals(StringUtils.EMPTY)) {
								text = refactorText(text);

								Step step = new Step();
								step.setStepText(text);
								step.setRicetta(ricetta);
								stepList.add(step);
							}
						}
					} else {
						for (Element element : liStepList) {
							String text = element.text();
							if (text != null && !text.trim().equals(StringUtils.EMPTY)) {
								text = refactorText(text);
								Step step = new Step();
								step.setStepText(text);
								step.setRicetta(ricetta);
								stepList.add(step);
							}
						}
					}

					if (stepList.size() > 0) {
						ricetta.setStepList(stepList);

						Element elementsByID = doc.body().getElementById("ingredient-section");
						Elements liIngredientSection = elementsByID.getElementsByAttributeValue("itemprop",
								"recipeIngredient");
						List<Ingrediente> ingredienti = new ArrayList<>();
						for (Element element : liIngredientSection) {
							String text = element.text();
							if (text != null && !element.text().trim().equals(StringUtils.EMPTY)) {
								Ingrediente i = new Ingrediente();
								text = refactorText(text);

								i.setText(text);
								i.setRicetta(ricetta);
								ingredienti.add(i);
							}
						}
						if (ingredienti.size() > 0) {
							ricetta.setIngredienti(ingredienti);

							ricettaRepository.save(ricetta);
						}
					}
				}
			}
		}
		return true;
	}

	private String refactorText(String text) {
		text = StringUtils.replace(text, "vel ", "velocità ");
		text = StringUtils.replace(text, "vel.", "velocità ");
		text = StringUtils.replace(text, "sec ", "secondi ");
		text = StringUtils.replace(text, "sec.", "secondi ");
		text = StringUtils.replace(text, "min ", "minuti ");
		text = StringUtils.replace(text, "min.", "minuti ");
		text = StringUtils.replace(text, "nelboccale", "nel boccale");
		text = StringUtils.replace(text, "/", " ");
		text = StringUtils.replace(text, "\"", "secondi");
		text = StringUtils.replace(text, "qb", "quanto basta");
		text = StringUtils.replace(text, "q.b.", "quanto basta");
		text = StringUtils.replace(text, "q.b", "quanto basta");
		text = StringUtils.replace(text, "qb.", "quanto basta");
		return text;
	}

}
