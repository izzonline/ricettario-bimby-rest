package it.izzonline.ricettario.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.izzonline.ricettario.objects.Ricetta;

public interface RicettaRepository extends JpaRepository<Ricetta, Long> {

	Ricetta findByUuid(String uuid);

	Ricetta findByTitle(String title);

	Ricetta findByTitleAndCategoria(String title, String categoria);

	Ricetta findByWpId(Long wpId);

	List<Ricetta> findByWpIdIsNull();

	List<Ricetta> findByTitleContaining(String title, Pageable pageRequest);

	List<Ricetta> findByTitleStartingWith(String title, Pageable pageRequest);

	List<Ricetta> findByTitleContainingAndCategoria(String title, String categoria, Pageable pageRequest);

	List<Ricetta> findByTitleStartingWithAndCategoria(String title, String categoria, Pageable pageRequest);

	@Query(value = "select distinct(categoria) from ricette.ricetta order by categoria asc", nativeQuery = true)
	List<String> findCategorie();
}
