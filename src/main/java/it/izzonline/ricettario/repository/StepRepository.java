package it.izzonline.ricettario.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import it.izzonline.ricettario.objects.Step;

public interface StepRepository extends JpaRepository<Step, Long> {
	
	
	@Modifying
	@Query(value="delete from ricette.step where ricetta_id not in = (select id from ricette.ricetta)", nativeQuery=true)
	void deleteOrphans();
	

}
